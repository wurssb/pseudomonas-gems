# Pseudomonas GEMs

Repository for the following article: Comparative genome-scale constraint-based metabolic modeling reveals key lifestyle features of plant-associated *Pseudomonas* spp. 

## Usage
The repository contains the "Materials" and the "Supplementary files" for the publication.

### Materials
The repository contains scripts, python and R, and the materials to replicate the analysis.
 - "blolog" contains the comparison of carbon utilization profile from model prediction to the biolog data.
 - "compositional_analysis" contains the compositional analysis of all 108 generated models and the wordcloud representation.
 - "models" contains all 108 generated models using CarveMe.
 - "selected_strains" 
    - "composition_d-ornithine" contains the compositional analysis of the 12 representative models and an in-depth analysis of the d-ornithine annotation.
    - "simulation" contains simulation scripts for essentiality and flux sampling analysis.
    - "strains_selection" contains the methods to selected for the 12 representative strains.

## Publication
Poncheewin, W., van Diepeningen, A.D., van der Lee, T.A.J. et al. 
"Comparative genome-scale constraint-based metabolic modeling reveals key lifestyle features of plant-associated *Pseudomonas* spp."
bioRixv (2022). https://doi.org/10.1101/2022.07.26.501552

## License
This work is licensed under the MIT license.
