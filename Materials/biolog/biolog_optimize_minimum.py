# !/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Author: Wasin Poncheewin
Script to simulate the oxidation from the biolog data
"""

import cobra
import cobra.test
from cobra import Model, Reaction, Metabolite
from cobra.flux_analysis import flux_variability_analysis
from cobra.flux_analysis import production_envelope
from cobra.util.solver import linear_reaction_coefficients

import os
from os import listdir
from os.path import isfile, join
import sys
import pandas as pd


if __name__ == '__main__':

	# folder with the sequenced strains (the PGPR models that starts with lbc_*.xml)
	folder = './'

	# List of Biolog compounds
	biolog = ["ser__D","g6p","asn__L","gln__L","arab__L","sbt__D","asp__D","cit","tcb","acgam","glyc","g1p","ser__L","fuc__L","rib__D","f6p","thr__L","tym","succ","glcur","fum","ala__L","gal","glcn","rmn","lyx__L","asp__L","fru","acac","pro__L","xyl__D","ac","pyr","ala__D","lac__L","Glc_aD","for","glx","galur","man","mnl","mal__D","glu__L","uri","adn","ins","mal__L","acgala","ad","acnam","malon","phe__L","citac","acglu","citm","oxa","arg__L","val__L","stys","gam","gly","dextrin","abt__D","tag__D","quin","his__L","4hbz","hom__L","glycogen","ptrc","ghb","ile__L","dha","leu__L","mannan","lys__L","pectin","met__L"]
	biolog += ['orn']

	# Prepare output file
	outFile = open('model_biolog_minimize.tsv', 'w')
	outFile.write('Model' + '\t' + 'Csource' +'\t' + 'Growth_glc' + '\t' +  'minimized_c_flux' + '\n') 
	
	onlyfiles = [f for f in listdir(folder) if isfile(join(folder, f))]
	
	for f in onlyfiles:
		if f.endswith('.xml'):
			print (f)
			modelname = folder + f
			model = cobra.io.read_sbml_model(modelname)
			solution = model.optimize()
			for b in biolog:
				print (b)
				if (b+'_e') in model.metabolites:
					csource = 'EX_' + b + '_e'
					model.reactions.get_by_id(csource).lower_bound = -10
					print (model.medium) # Check medium for additional metabolite

					solution = model.optimize()
					print ("Growth:" + str(solution.objective_value)) # Report growth

					model.objective = csource # Change model's objective to the carbon source
					uptaken = model.optimize(objective_sense = "minimize") # Optimize
					print (csource + ": " + str(uptaken.objective_value)) # Report consumption

					model.reactions.get_by_id(csource).lower_bound = 0 # Reset the carbon
					# Write to output file
					outFile.write(f.replace('.xml','') + '\t' + b + '\t' + str(solution.objective_value) + '\t' + str(uptaken.objective_value) + '\n')
					
				else:
					print (b+'_e not in metabolite list')

	outFile.close()
