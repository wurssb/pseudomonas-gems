import subprocess

strain_list = []
numRound = 1000
numStrain = 6
for i in range(numRound):
	print ('round', i)
	cmd = "python3 Treemmer-master/Treemmer_v0.3.py beneficial.newick -X " + str(numStrain)
	subprocess.check_output(cmd, shell = True)

	with open("beneficial.newick_trimmed_list_X_" + str(numStrain)) as inFile:
		for line in inFile:
			strain = line.strip()
			strain_list += [strain]

outFile = open('strain_list_beneficial_' + str(numRound) + '.txt', 'w')
counter = 1
n = 1
for s in strain_list:
	outFile.write(s + '\t' + str(n) + '\n')
	counter += 1
	if counter > numStrain:
		counter = 1
		n += 1
outFile.close()
