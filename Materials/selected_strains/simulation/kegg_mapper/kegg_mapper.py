# !/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Author: Wasin Poncheewin
Script to perform kegg mapper
"""

import os
from os import listdir, makedirs
from os.path import isfile, join
import sys
import pandas as pd

if __name__ == '__main__':

	kegg_map_ec = {}
	with open('kegg_map_ec.txt') as kegg_map:
		for line in kegg_map:
			content = line.strip().split("\t")
			if len(content) > 1:
				map_id, map_ec =  line.strip().split("\t")
				if map_id not in kegg_map_ec:
					kegg_map_ec[map_id] = [map_ec]
				else:
					kegg_map_ec[map_id] += [map_ec]

	# selected result folder from "ess_reaction_to_ec.py"
	folder = '../'
	onlyfiles = [f for f in listdir(folder) if isfile(join(folder, f))]

	strains_ec = {}
	for f in onlyfiles:
		if f.endswith('_ec.tsv'):
			filename = folder + f
			with open (filename) as inputFile:
				for line in inputFile:
					ec = line.strip()
					ec_num = 'ec:'+ec
					if f not in strains_ec:
						strains_ec[f] = [ec_num]
					else:
						if ec_num not in strains_ec[f]:
							strains_ec[f] += [ec_num]
							
	outFile = open('kegg_mapper.tsv', 'w')
	for path in kegg_map_ec:
		for strain in strains_ec:
			count = 0
			for ec in strains_ec[strain]:
				if ec in kegg_map_ec[path]:
					count += 1
			line = (strain + '\t' + path + '\t' + str(count) + '\n')
			outFile.write(line)
	outFile.close()

	# Add details to the kegg mapper results
	kegg_map_info = {}
	with open('kegg_map_with_count.tsv') as kegg_map:
		for line in kegg_map:
			if not line.startswith("map"):
				map_id, map_detail, map_ev_count = line.strip().split('\t')
				if map_id not in kegg_map_info:
					kegg_map_info[map_id] = [map_detail, map_ev_count]
	
	outFile = open('kegg_mapper_details.tsv', 'w')
	with open('kegg_mapper.tsv') as inputFile:
		for line in inputFile:
			strain, map_id, count = line.strip().split('\t')
			if int(count) > 0:
				strain = strain.replace('_ec.tsv','')
				line = strain + '\t' + map_id + '\t' + kegg_map_info[map_id][0] + '\t' + str(count) + '\t' + kegg_map_info[map_id][1] + '\n'
				outFile.write(line)
	outFile.close()
		