# !/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Author: Wasin Poncheewin
Script to extract ec from essential reactions
"""

import os
from os.path import join
from os import listdir
from os.path import isfile, join
import sys
import pandas as pd

import cobra
from cobra.flux_analysis import flux_variability_analysis
from cobra.flux_analysis import production_envelope
import cobra.test
from cobra import Model, Reaction, Metabolite
from cobra.util.solver import linear_reaction_coefficients

if __name__ == '__main__':

	# select generated "srd_ess_*" folder from the essential_analysis.R results
	folder = sys.argv[1]
	onlyfiles = [f for f in listdir(folder) if isfile(join(folder, f))]

	for f in onlyfiles:
		if f.endswith('.txt'):
			print (f)
			filename = folder + f
			selected_r = []
			with open(filename) as inputFile:
				for line in inputFile:
					selected_r += [line.strip()]

			outFile_fullinfo = open(folder+f.replace('.txt','_summary.csv'), 'w')
			outFile_ec = open(folder+f.replace('.txt','_ec.tsv'),'w')
			outFile_ec_prefix = open(folder+f.replace('.txt','_ec_prefix.tsv'),'w')

			modelname = '../' + f.replace('.txt','.xml')
			model = cobra.io.read_sbml_model(modelname)

			for r in model.reactions:
				if r.id in selected_r:
					if 'ec-code' in r.annotation.keys():
						if (isinstance(r.annotation['ec-code'], list)):
							print (r.id + '\t' + r.name + '\t' + ', '.join(r.annotation['ec-code']))
							line = r.id + '\t' + r.name + '\t' + ', '.join(r.annotation['ec-code']) + '\t' + str(len(r.annotation['ec-code']))
							for ec in r.annotation['ec-code']:
								outFile_ec.write(ec + '\n')
								outFile_ec_prefix.write('EC'+ ec + '\n')
						else:
							print (r.id + '\t' + r.name + '\t' + r.annotation['ec-code'])
							line = r.id + '\t' + r.name + '\t' + r.annotation['ec-code'] + '\t' + str(1)
							outFile_ec.write(r.annotation['ec-code'] + '\n')
							outFile_ec_prefix.write('EC' + r.annotation['ec-code'] + '\n')
					else:
						print (r.id + '\t' + r.name + '\t' + 'no EC')
						line = r.id + '\t' + r.name + '\t' + 'no EC' + '\t' + str(0)
				else:
					line = r.id + '\t' + r.name + '\t' + 'not in model' + '\t' + str(0)
				outFile_fullinfo.write(line + '\n')
			outFile_fullinfo.close()
			outFile_ec.close()
			outFile_ec_prefix.close()
				


