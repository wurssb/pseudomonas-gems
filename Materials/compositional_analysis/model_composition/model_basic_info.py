# !/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Author: Wasin Poncheewin
Script to retrived basic information from the model
"""

import cobra
from cobra.flux_analysis import flux_variability_analysis
from cobra.flux_analysis import production_envelope
import cobra.test
from cobra import Model, Reaction, Metabolite
import os
from os.path import join
from os import listdir
from os.path import isfile, join
import sys
import pandas as pd

from cobra.util.solver import linear_reaction_coefficients

if __name__ == '__main__':

	

	outFile_basic = open('model_basic_info.tsv', 'w')
	outFile_reactions = open('model_list_reactions.tsv', 'w')
	outFile_metabolites = open('model_list_metabolites.tsv', 'w')
	outFile_genes = open('model_list_genes.tsv', 'w')
	
	outFile_basic.write('Model' + '\t' + 'Biomass_aero' + '\t' + 'Biomass_anaero' + '\t' + 'NumReactions'  + '\t' + 'NumMetabolites' + '\t' + 'NumGenes' + '\n') 
	
	# models folder
	folder = './'
	onlyfiles = [f for f in listdir(folder) if isfile(join(folder, f))]
	
	for f in onlyfiles:
		if f.endswith('.xml'):
			print (f)
			modelname = folder + f
			model = cobra.io.read_sbml_model(modelname)

			solution_aero = model.optimize()
			print ("Biomass w O2:" + str(solution_aero.objective_value))

			model.reactions.get_by_id('EX_o2_e').lower_bound = 0
			solution_anaero = model.optimize()
			print ("Biomass wo O2:" + str(solution_anaero.objective_value))

			outFile_basic.write(f.replace('.xml','') + '\t' + str(solution_aero.objective_value) + '\t' + str(solution_anaero.objective_value) + '\t' + str(len(model.reactions)) + '\t' + str(len(model.metabolites)) + '\t' + str(len(model.genes)) + '\n')
			
			model.reactions.get_by_id('EX_o2_e').lower_bound = -10

			print ("#Reactions: " + str(len(model.reactions)))
			for r in model.reactions:
				print (str(r))
				if (r.genes) :
					g = (','.join(str(g) for g in r.genes))
					status = 'GPRs'
				else:
					g = ""
					status = 'orphan'
				if "EX_" in str(r):
					status = 'exchange'
				outFile_reactions.write(f.replace('.xml','') + '\t' + str(r) + '\t' + status + '\n')

			print ("#Metabolites: " + str(len(model.metabolites)))
			for m in model.metabolites:
				print (str(m))
				if str(m).endswith('_c'):
					status = 'cytosol'
				elif str(m).endswith('_p'):
					status = 'periplasm'
				elif str(m).endswith('_e'):
					status = 'extracellular'
				else:
					status = ''
				outFile_metabolites.write(f.replace('.xml','') + '\t' + str(m) + '\t' + status + '\n')

			print ("#Genes: " + str(len(model.genes)))
			for g in model.genes:
				print (str(g))
				outFile_genes.write(f.replace('.xml','') + '\t' + str(g) + '\n')

	outFile_basic.close()
	outFile_reactions.close()
	outFile_metabolites.close()
	outFile_genes.close()



  
