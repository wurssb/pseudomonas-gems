# !/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Author: Wasin Poncheewin
Script to retrived GPR
"""

import cobra
from cobra.flux_analysis import flux_variability_analysis
from cobra.flux_analysis import production_envelope
import cobra.test
from cobra import Model, Reaction, Metabolite
import os
from os.path import join
from os import listdir
from os.path import isfile, join
import sys
import pandas as pd

from cobra.util.solver import linear_reaction_coefficients

if __name__ == '__main__':

	outFile_reactions = open('model_list_reactions_genes.tsv', 'w')

	# PGPR folder
	folder = './'
	onlyfiles = [f for f in listdir(folder) if isfile(join(folder, f))]
	for f in onlyfiles:
		if f.endswith('.xml'):
			print (f)
			modelname = folder + f
			model = cobra.io.read_sbml_model(modelname)
			print ("#Reactions: " + str(len(model.reactions)))
			for r in model.reactions:
				print (str(r))
				g = (','.join(str(g) for g in r.genes))
				outFile_reactions.write(f.replace('.xml','') + '\t' + str(r) + '\t' + g + '\t' + 'beneficial ' + '\n')

	# EPP folder
	folder = './'
	onlyfiles = [f for f in listdir(folder) if isfile(join(folder, f))]
	for f in onlyfiles:
		if f.endswith('.xml'):
			print (f)
			modelname = folder + f
			model = cobra.io.read_sbml_model(modelname)
			print ("#Reactions: " + str(len(model.reactions)))
			for r in model.reactions:
				print (str(r))
				g = (','.join(str(g) for g in r.genes))
				outFile_reactions.write(f.replace('.xml','') + '\t' + str(r) + '\t' + g + '\t' + 'pathogen ' + '\n')


	outFile_reactions.close()



  
